# Task manager

Task manager is a Java based web application that allows users to manage their tasks.

![](https://i.imgur.com/KmDzSeD.png)

## Functionalities implemented
* Log in the application with users defined in database
* Create tasks
* Show undone and undeleted tasks
* Delete/undelete task
* Mark task as completed/uncompleted
* Show tasks divided into groups: urgent/not urgent/important/not important
* Set/change priority of the task
* Force task to be urgent regardless the planned end date (by default task is marked as urgent if there are less than 4 days till planned end date)
* Add comments to tasks
* Change number of days when task becomes urgent
* Allow admin user do activate/deactivate users
* Change user name/surname/email

## Functionalities todo

## Technologies used

* Java
* Spring (MVC, Security)
* Hibernate
* Maven
* MySQL
* Bootstrap
* HTML/CSS
* jQuery

