package com.mateuszbochenski.management;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mateuszbochenski.user.User;
import com.mateuszbochenski.user.UserService;

@Controller
public class ManagementController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/management", method=RequestMethod.GET)
	String showManagement(ModelMap model) {
		
		Collection<User> users = userService.retrieveUsers();
		model.addAttribute("users", users);
		
		return "management";
	}
	
	@RequestMapping(value="/activate-user", method=RequestMethod.GET)
	String activateUser(ModelMap model, @RequestParam long id) {
		
		User user = userService.retreiveUser(id);
		userService.activate(user);
		
		return "redirect:management";
	}
	
	@RequestMapping(value="/deactivate-user", method=RequestMethod.GET)
	String deactivateUser(ModelMap model, @RequestParam long id) {
		
		User user = userService.retreiveUser(id);
		userService.deactivate(user);
		
		return "redirect:management";
	}
}
