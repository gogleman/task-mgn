package com.mateuszbochenski.settings;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mateuszbochenski.user.User;
import com.mateuszbochenski.user.UserService;

@Controller
public class SettingController {

	@Autowired
	SettingService settingService;

	@Autowired
	UserService userService;

	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public String showSettings(ModelMap model, ServletRequest request) {

		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		User currentUser = userService.retreiveUser(login);
		Setting numberOfUrgentDaysSetting = settingService.retrieveSetting(SettingType.DAYS_FOR_URGENT_TASK,
				currentUser);
		
		SettingsWrapper wrapper = new SettingsWrapper(numberOfUrgentDaysSetting, currentUser);
		model.addAttribute("settingsWrapper", wrapper);
		
		Map<String, String[]> paramMap = request.getParameterMap();
		if (paramMap.containsKey("settingsSaved")) {
			model.addAttribute("settingsSaved", "true");
		}

		return "settings";
	}

	@RequestMapping(value = "/settings", method = RequestMethod.POST)
	public String saveSettings(ModelMap model, @Valid SettingsWrapper settingsWrapper, BindingResult result) {

		if (result.hasErrors()) {
			model.addAttribute("bindingError", "true");
			return "/settings";
		}
		
		String login = SecurityContextHolder.getContext().getAuthentication().getName();
		User currentUser = userService.retreiveUser(login);
		
		settingsWrapper.getUser().setRoles(currentUser.getRoles());
		settingsWrapper.getUser().setSettings(currentUser.getSettings());
		
		userService.update(settingsWrapper.getUser());
		settingService.update(settingsWrapper.getSetting());

		model.clear();
		return "redirect:/settings?settingsSaved=true";
	}

}
