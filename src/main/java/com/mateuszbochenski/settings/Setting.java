package com.mateuszbochenski.settings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.mateuszbochenski.user.User;

@Entity
@Table(name="USER_SETTINGS")
public class Setting {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="USER_SETTING_ID")
	private Long id;
	@Enumerated(EnumType.STRING)
	private SettingType type;
	private String valueString;
	@Min(value=0, message="Enter integer number between 0 and 365") 
	@Max(value=365, message="Enter integer number between 0 and 365")
	private int valueInt;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn (name="USER_ID")
	private User user;
	
	public Setting() {
	}

	public Setting(SettingType type, String valueString, User user) {
		this.type = type;
		this.valueString = valueString;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SettingType getType() {
		return type;
	}

	public void setType(SettingType type) {
		this.type = type;
	}

	public String getValueString() {
		return valueString;
	}

	public void setValueString(String valueString) {
		this.valueString = valueString;
	}
	
	public int getValueInt() {
		return valueInt;
	}

	public void setValueInt(int valueInt) {
		this.valueInt = valueInt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Setting other = (Setting) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserSetting [id=" + id + ", type=" + type + ", valueString=" + valueString + ", valueInt=" + valueInt + "]";
	}

}
