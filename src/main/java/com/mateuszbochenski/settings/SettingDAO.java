package com.mateuszbochenski.settings;

import com.mateuszbochenski.user.User;

public interface SettingDAO {
	
	void add(Setting setting);
	Setting update (Setting setting);
	void delete (Setting setting);
	Setting findByType(SettingType type);
	Setting retrieveSetting(SettingType type, User user);
	

}
