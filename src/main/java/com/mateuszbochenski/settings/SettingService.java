package com.mateuszbochenski.settings;

import com.mateuszbochenski.user.User;

public interface SettingService {

	void add(Setting setting);
	Setting update (Setting setting);
	void delete (Setting setting);
	Setting retrieveSetting(SettingType type, User user);
	int retrieveSettingValueInt(SettingType type, User user);
	int retrieveNumberOfUrgentDays(User user);
	

}
