package com.mateuszbochenski.settings;

import javax.validation.Valid;

import com.mateuszbochenski.user.User;

public class SettingsWrapper {
	@Valid
	private Setting setting;
	@Valid
	private User user;
	
	public SettingsWrapper() {
	}
	
	
	public SettingsWrapper(Setting setting, User user) {
		this.setting = setting;
		this.user = user;
	}


	public Setting getSetting() {
		return setting;
	}
	public void setSetting(Setting setting) {
		this.setting = setting;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

}
