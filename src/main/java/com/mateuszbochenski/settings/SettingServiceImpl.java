package com.mateuszbochenski.settings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mateuszbochenski.user.User;

@Service
public class SettingServiceImpl implements SettingService {
	
	@Autowired
	SettingDAO settingDAO;
	
	@Override
	public void add(Setting setting) {
		settingDAO.add(setting);	
	}

	@Override
	public Setting update(Setting setting) {
		return settingDAO.update(setting);
	}

	@Override
	public void delete(Setting setting) {
		settingDAO.delete(setting);	
	}

	@Override
	public Setting retrieveSetting(SettingType type, User user) {
		return settingDAO.retrieveSetting(type, user);
	}

	@Override
	public int retrieveSettingValueInt(SettingType type, User user) {
		Setting setting =  settingDAO.retrieveSetting(type, user);
		return setting.getValueInt();
	}

	@Override
	public int retrieveNumberOfUrgentDays(User user) {
		Setting setting = settingDAO.retrieveSetting(SettingType.DAYS_FOR_URGENT_TASK,user);
		return setting.getValueInt();
	}

}
