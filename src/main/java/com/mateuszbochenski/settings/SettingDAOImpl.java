package com.mateuszbochenski.settings;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mateuszbochenski.user.User;

@Repository
@Transactional
public class SettingDAOImpl implements SettingDAO {

	@PersistenceContext
	EntityManager em;

	@Override
	public void add(Setting setting) {
		em.persist(setting);

	}

	@Override
	public Setting update(Setting setting) {
		return em.merge(setting);
	}

	@Override
	public void delete(Setting setting) {
		em.remove(em.contains(setting) ? setting : em.merge(setting));
	}

	@Override
	public Setting findByType(SettingType type) {
		return em.find(Setting.class, type);
	}

	@Override
	public Setting retrieveSetting(SettingType type, User user) {
		return (Setting) em.createQuery("SELECT s from Setting s where s.type = :type and user = :user")
				.setParameter("type", type)
				.setParameter("user", user)
				.getSingleResult();
	}

}
