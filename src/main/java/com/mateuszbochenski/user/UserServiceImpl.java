package com.mateuszbochenski.user;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mateuszbochenski.task.Task;
import com.mateuszbochenski.task.TaskDAO;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	TaskDAO taskDAO;
	
	
	@Override
	public void add(User user) {
		userDAO.add(user);
	}

	@Override
	public void add(String login, String password, String firstName, String lastName, String email, UserState state,
			Collection<Role> roles, Set<Task> tasks, boolean enabled) {
		User user = new User(login,password,firstName,lastName, email, state, roles, tasks, enabled);
		userDAO.add(user);
	}

	@Override
	public void activate(User user) {
		user.setEnabled(true);
		userDAO.update(user);
	}

	@Override
	public void deactivate(User user) {
		user.setEnabled(false);
		userDAO.update(user);
	}

	@Override
	public void deletePermanently(User user) {
		userDAO.delete(user);
	}

	@Override
	public User retreiveUser(Long id) {
		return userDAO.findById(id);
	}

	@Override
	public User retreiveUser(String login) {
		return userDAO.findByLogin(login);
	}

	@Override
	public void addTaskToUser(User user, Task task) {
		Set<Task> tasks = user.getTasks();
		if (tasks == null) tasks = new HashSet<Task>();
		tasks.add(task);
		
		Set<User> users = task.getUsers();
		if (users == null) users = new HashSet<User>();
		users.add(user);
		
		taskDAO.update(task);
		userDAO.update(user);
		
	}

	@Override
	public User update(User user) {
		return userDAO.update(user);
	}

	@Override
	public Collection<User> retrieveUsers() {
		return userDAO.findAll();
	}
	

}
