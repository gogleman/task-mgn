package com.mateuszbochenski.user;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void add(User user) {
		em.persist(user);	
	}

	@Override
	public void delete(User user) {
		em.remove(em.contains(user) ? user : em.merge(user));
	}

	@Override
	public User update(User user) {
		return em.merge(user);
	}

	@Override
	public User findById(long id) {
		return em.find(User.class, id);
	}

	@Override
	public User findByLogin(String login) {
		TypedQuery<User> q = em.createQuery("select u from User u where login like :userLogin",User.class);
		q.setParameter("userLogin", login);
		
		return q.getSingleResult();
	}

	@Override
	public List<User> findAll() {
		TypedQuery<User> q = em.createQuery("select u from User u",User.class);
		return q.getResultList();
	}

}
