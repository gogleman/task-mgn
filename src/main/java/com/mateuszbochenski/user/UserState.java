package com.mateuszbochenski.user;

public enum UserState {
	REGISTERED,
	ACTIVE,
	BLOCKED,
	DEACTIVATED
}
