package com.mateuszbochenski.user;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.mateuszbochenski.settings.Setting;
import com.mateuszbochenski.task.Task;

@Entity
@Table(name="USERS")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private Long id;
	@Column(nullable=false, unique=true,length=45)
	private String login;
	@Column(nullable=false, length=60)
	private String password;
	@Column(length=60)
	@Size(min=2,message="Enter at least 2 characters")
	private String firstName;
	@Column(length=60)
	@Size(min=2,message="Enter at least 2 characters")
	private String lastName;
	@Column(nullable=false, length=60)
	private String email;
	@Column
	private boolean enabled;
	@Column
	private UserState state;
	@Column
	@ManyToMany(fetch = FetchType.EAGER, cascade= CascadeType.ALL)
	@JoinTable(
			name="USERS_ROLES", 
			joinColumns= @JoinColumn(name = "user_id", referencedColumnName = "user_id"),
			inverseJoinColumns = @JoinColumn(name="role_id", referencedColumnName = "role_id"))
	private Collection<Role> roles;
	@ManyToMany(fetch = FetchType.EAGER, cascade= CascadeType.MERGE)
	@JoinTable(
			name="USERS_TASKS", 
			joinColumns= @JoinColumn(name = "USER_ID", referencedColumnName = "user_id"),
			inverseJoinColumns = @JoinColumn(name="TASK_ID", referencedColumnName = "task_id"))
	private Set<Task> tasks;
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL,mappedBy="user")
	private Set<Setting> settings;
	
	
	
	public User() {
	}

	public User(String login, String password, String firstName, String lastName, String email, UserState state,
			Collection<Role> roles, Set<Task> tasks, boolean enabled) {
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.state = state;
		this.roles = roles;
		this.tasks = tasks;
		this.enabled = enabled;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public UserState getState() {
		return state;
	}
	public void setState(UserState state) {
		this.state = state;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public Set<Task> getTasks() {
		return tasks;
	}
	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<Setting> getSettings() {
		return settings;
	}

	public void setSettings(Set<Setting> settings) {
		this.settings = settings;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", login=" + login + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", enabled=" + enabled + ", state=" + state
				+ ", roles=" + roles +  "]";
	}
	
}
