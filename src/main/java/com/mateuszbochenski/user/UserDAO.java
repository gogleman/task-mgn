package com.mateuszbochenski.user;

import java.util.List;

public interface UserDAO {
	
	void add(User user);
	void delete(User user);
	User update(User user);
	User findById(long id);
	User findByLogin(String login);
	List<User> findAll();
}
