package com.mateuszbochenski.user;

import java.util.Collection;
import java.util.Set;

import com.mateuszbochenski.task.Task;

public interface UserService {
	void add(User user);
	void add(String login, String password, String firstName, String lastName, String email, UserState state,
			Collection<Role> roles, Set<Task> tasks, boolean enabled);
	void addTaskToUser(User user, Task task);
	void activate(User user);
	void deactivate(User user);
	void deletePermanently(User user);
	User update(User user);
	
	User retreiveUser(Long id);
	User retreiveUser(String login);
	Collection<User> retrieveUsers();
}
