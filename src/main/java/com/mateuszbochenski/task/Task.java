package com.mateuszbochenski.task;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.mateuszbochenski.user.User;

@Entity
@Table(name="TASKS")
public class Task {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TASK_ID")
	private Long id;
	@Column(nullable=false)
	@Size(min = 6, message = "Enter at least 6 characters")
	private String name; 
	private String description;
	@Column(nullable=false)
	private Date creationDate;
	@Column(nullable=false)
	private String creatorLogin;
	private boolean isImportant;
	private Date plannedEndDate;
	private Date finalEndDate;
	private boolean isCompleted;
	private boolean isDeleted;
	private boolean isForcedUrgent;
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "tasks")
	private Set<User> users;
	@OneToMany(cascade=CascadeType.ALL,mappedBy="task")
	private Set<TaskComment> comments;
	
	public Task() {
	}
	
	public Task(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public Task(Long id, String name, String description, Date creationDate, String creatorLogin, boolean isImportant,
			Date plannedEndDate, Date finalEndDate, boolean isDone, boolean isDeleted, boolean isForcedUrgent, Set<User> users) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.creationDate = creationDate;
		this.creatorLogin = creatorLogin;
		this.isImportant = isImportant;
		this.plannedEndDate = plannedEndDate;
		this.finalEndDate = finalEndDate;
		this.isCompleted = isDone;
		this.isDeleted = isDeleted;
		this.isForcedUrgent = isForcedUrgent;
		this.users = users;
	}
	
	public Task(String name, String description, Date creationDate, String creatorLogin, boolean isImportant,
			Date plannedEndDate, Date finalEndDate, boolean isDone, boolean isDeleted, boolean isForcedUrgent, Set<User> users) {
		this.name = name;
		this.description = description;
		this.creationDate = creationDate;
		this.creatorLogin = creatorLogin;
		this.isImportant = isImportant;
		this.plannedEndDate = plannedEndDate;
		this.finalEndDate = finalEndDate;
		this.isCompleted = isDone;
		this.isDeleted = isDeleted;
		this.isForcedUrgent = isForcedUrgent;
		this.users = users;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatorLogin() {
		return creatorLogin;
	}

	public void setCreatorLogin(String creatorLogin) {
		this.creatorLogin = creatorLogin;
	}

	public Date getPlannedEndDate() {
		return plannedEndDate;
	}

	public void setPlannedEndDate(Date plannedEndDate) {
		this.plannedEndDate = plannedEndDate;
	}

	public Date getFinalEndDate() {
		return finalEndDate;
	}

	public void setFinalEndDate(Date finalEndDate) {
		this.finalEndDate = finalEndDate;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public boolean isImportant() {
		return isImportant;
	}

	public void setImportant(boolean isImportant) {
		this.isImportant = isImportant;
	}

	public boolean isForcedUrgent() {
		return isForcedUrgent;
	}

	public void setForcedUrgent(boolean isForcedUrgent) {
		this.isForcedUrgent = isForcedUrgent;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<TaskComment> getComments() {
		return comments;
	}

	public void setComments(Set<TaskComment> comments) {
		this.comments = comments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", name=" + name + ", description=" + description + ", creationDate=" + creationDate
				+ ", creatorLogin=" + creatorLogin + ", isImportant=" + isImportant + ", plannedEndDate="
				+ plannedEndDate + ", finalEndDate=" + finalEndDate + ", isCompleted=" + isCompleted + ", isDeleted="
				+ isDeleted + ", isForcedUrgent=" + isForcedUrgent + ", users=" + users + ", comments=" + comments
				+ "]";
	}


}
