package com.mateuszbochenski.task;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.mateuszbochenski.user.User;

public interface TaskService {
	void add(Task task);
	void add(String name, String description, Date creationDate, String creatorLogin, boolean isImportant, Date plannedEndDate, Date finalEndDate, boolean isDone, boolean isDeleted, boolean isForcedUrgent, Set<User> users);

	void deleteTaskTemporarly(Task task);
	void undeleteTaskTemporarly(Task task);
	void deleteTaskPermanently(Task task);
	void completeTask(Task task);
	void uncompleteTask(Task task);
	
	void editTask(Task task);

	Task retrieveTask(long id);
	List<Task> retrieveUndoneUndeletedTasks(long userId);
	List<Task> retrieveUndoneUndeletedTasksUrgentImportant(long userId);
	List<Task> retrieveUndoneUndeletedTasksUrgentNotImportant(long userId);
	List<Task> retrieveUndoneUndeletedTasksNotUrgentImportant(long userId);
	List<Task> retrieveUndoneUndeletedTasksNotUrgentNotImportant(long userId);

	List<Task> retrieveDoneUndeletedTasks(long userId, int numberOfTasks);
	List<Task> retrieveDeletedTasks(long userId, int numberOfTasks);
	
	void addComment(TaskComment taskComment);
	void addCommentToTask(TaskComment taskComment, Task task);
	List<TaskComment> retrieveTaskComments(Task task);
	
}
