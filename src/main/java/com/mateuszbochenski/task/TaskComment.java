package com.mateuszbochenski.task;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="COMMENTS")
public class TaskComment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COMMENT_ID")
	private Long id;
	@Column(nullable=false)
	@Size(min = 2, message = "Enter at least 2 characters")
	private String text;
	@Column(nullable=false)
	private String creatorLogin;
	@Column(nullable=false)
	private Date creationDate;
	@ManyToOne()
	@JoinColumn(name="TASK_ID", nullable=false)
	private Task task;
	
	
	public TaskComment() {
	}
	
	public TaskComment(String text, String creatorLogin, Date creationDate, Task task) {
		this.text = text;
		this.creatorLogin = creatorLogin;
		this.creationDate = creationDate;
		this.task = task;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCreatorLogin() {
		return creatorLogin;
	}
	public void setCreatorLogin(String creatorLogin) {
		this.creatorLogin = creatorLogin;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskComment other = (TaskComment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TaskComment [id=" + id + ", text=" + text + ", creatorLogin=" + creatorLogin + ", creationDate="
				+ creationDate + ", task=" + task + "]";
	}
	
	
}
