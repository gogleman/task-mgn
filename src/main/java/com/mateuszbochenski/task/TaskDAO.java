package com.mateuszbochenski.task;

import java.util.List;

public interface TaskDAO {
	
	void add(Task task);
	void delete(Task task);
	Task update(Task task);
	Task findById(long id);
	
	List<Task> retrieveUndoneUndeletedTasks();
	List<Task> retrieveDoneUndeletedTasks(int numberOfTasks);
	List<Task> retrieveDeletedTasks(int numberOfTasks);

}
