package com.mateuszbochenski.task;

import java.util.List;

public interface TaskCommentDAO {
	
	void add(TaskComment taskComment);
	void delete(TaskComment taskComment);
	TaskComment update(TaskComment taskComment);
	TaskComment findById(long id);
	
	List<TaskComment> retrieveTaskCommentsByTask(Task task); 
	List<TaskComment> retrieveTaskCommentsByTaskDesc(Task task); 

}
