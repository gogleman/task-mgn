package com.mateuszbochenski.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.mateuszbochenski.settings.SettingService;
import com.mateuszbochenski.user.User;
import com.mateuszbochenski.user.UserService;

@Service
public class TaskServiceDBImpl implements TaskService{

	@Autowired
	TaskDAO taskDAO;
	
	@Autowired
	TaskCommentDAO taskCommentDAO;
	
	@Autowired
	UserService userService;
	
	@Autowired
	SettingService settingService;
	
	
	@Transactional
	@Override
	public void add(Task task) {
		taskDAO.add(task);
	}

	@Override
	public void add(String name, String description, Date creationDate, String creatorLogin, boolean isImportant,
			Date plannedEndDate, Date finalEndDate, boolean isDone, boolean isDeleted, boolean isForcedUrgent, Set<User> users) {
		taskDAO.add(new Task(name, description, creationDate, creatorLogin, isImportant, plannedEndDate, finalEndDate, isDone, isDeleted, isForcedUrgent, users));
	}

	@Override
	public void deleteTaskTemporarly(Task task) {
		task.setDeleted(true);
		task.setFinalEndDate(new Date());
		task = taskDAO.update(task);	
	}

	@Override
	public void undeleteTaskTemporarly(Task task) {
		task.setDeleted(false);
		task.setFinalEndDate(null);
		task = taskDAO.update(task);	
	}

	@Override
	public void deleteTaskPermanently(Task task) {
		taskDAO.delete(task);	
	}

	@Override
	public void completeTask(Task task) {
		task.setCompleted(true);
		task.setFinalEndDate(new Date());
		task = taskDAO.update(task);
	}

	@Override
	public void uncompleteTask(Task task) {
		task.setCompleted(false);
		task.setFinalEndDate(null);
		task = taskDAO.update(task);		
	}

	@Override
	public void editTask(Task task) {
		taskDAO.update(task);	
	}

	@Override
	public Task retrieveTask(long id) {
		return taskDAO.findById(id);
	}
	
	@Override
	public List<Task> retrieveUndoneUndeletedTasks(long userId) {
		return taskDAO.retrieveUndoneUndeletedTasks();
	}

	@Override
	public List<Task> retrieveDoneUndeletedTasks(long userId, int numberOfTasks) {
		return taskDAO.retrieveDoneUndeletedTasks(numberOfTasks);
	}

	@Override
	public List<Task> retrieveDeletedTasks(long userId, int numberOfTasks) {
		return taskDAO.retrieveDeletedTasks(numberOfTasks);
	}

	@Override
	public List<Task> retrieveUndoneUndeletedTasksUrgentImportant(long userId) {
		List<Task> tasks = taskDAO.retrieveUndoneUndeletedTasks();
		List<Task> filteredTasks = new ArrayList<Task>();
		
		for(Task task : tasks) {
			if (task.isImportant()  && isUrgent(task)) {
				filteredTasks.add(task);
			}
		}
		
		return filteredTasks;
	}

	@Override
	public List<Task> retrieveUndoneUndeletedTasksUrgentNotImportant(long userId) {
		List<Task> tasks = taskDAO.retrieveUndoneUndeletedTasks();
		List<Task> filteredTasks = new ArrayList<Task>();
		
		for(Task task : tasks) {
			if (task.isImportant() == false && isUrgent(task)) {
				filteredTasks.add(task);
			}
		}
		
		return filteredTasks;
	}

	@Override
	public List<Task> retrieveUndoneUndeletedTasksNotUrgentImportant(long userId) {
		List<Task> tasks = taskDAO.retrieveUndoneUndeletedTasks();
		List<Task> filteredTasks = new ArrayList<Task>();
		
		for(Task task : tasks) {
			if (task.isImportant() == true && !isUrgent(task)) {
				filteredTasks.add(task);
			}
		}
		
		return filteredTasks;
	}

	@Override
	public List<Task> retrieveUndoneUndeletedTasksNotUrgentNotImportant(long userId) {
		List<Task> tasks = taskDAO.retrieveUndoneUndeletedTasks();
		List<Task> filteredTasks = new ArrayList<Task>();
		
		for(Task task : tasks) {
			if (task.isImportant() == false && !isUrgent(task)) {
				filteredTasks.add(task);
			}
		}
		
		return filteredTasks;
	}
	
	private boolean isUrgent(Task task) {
		
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		User currentUser = userService.retreiveUser(login);
		
		int numOfUrgentDays = settingService.retrieveNumberOfUrgentDays(currentUser);

		Date date = new Date();
		long diffInMillies = task.getPlannedEndDate().getTime() - date.getTime();
		long daysTillDeadline = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

		if (daysTillDeadline+1<=numOfUrgentDays || task.isForcedUrgent()) 
			return true;
		return false;
	}
	
	public void addComment(TaskComment taskComment) {
		taskCommentDAO.add(taskComment);
	}
	
	public void addCommentToTask(TaskComment comment, Task task) {
		Set<TaskComment> comments = task.getComments();
		if (comments == null)
			comments = new HashSet<TaskComment>();
		comments.add(comment);
		task.setComments(comments);
	}

	@Override
	public List<TaskComment> retrieveTaskComments(Task task) {
		
		return taskCommentDAO.retrieveTaskCommentsByTaskDesc(task);
	}
	

}
