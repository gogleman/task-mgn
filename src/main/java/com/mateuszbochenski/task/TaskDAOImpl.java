package com.mateuszbochenski.task;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TaskDAOImpl implements TaskDAO{
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public void add(Task task) {
		em.persist(task);
	}

	@Override
	public void delete(Task task) {
		em.remove(em.contains(task) ? task : em.merge(task));
	}

	@Override
	public Task update(Task task) {
		return em.merge(task);
	}

	@Override
	public Task findById(long id) {
		return em.find(Task.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> retrieveUndoneUndeletedTasks() {
		
		return em.createQuery(
			    "SELECT t FROM Task t WHERE t.isCompleted = false and t.isDeleted = false")
			    .getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> retrieveDeletedTasks(int numberOfTasks) {
		return em.createQuery(
			    "SELECT t FROM Task t WHERE t.isDeleted = true ORDER BY finalEndDate DESC")
				.setMaxResults(numberOfTasks)
			    .getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> retrieveDoneUndeletedTasks(int numberOfTasks) {
		return em.createQuery(
			    "SELECT t FROM Task t WHERE t.isCompleted = true and t.isDeleted = false ORDER BY finalEndDate DESC")
				.setMaxResults(numberOfTasks)
			    .getResultList();
	}

}
