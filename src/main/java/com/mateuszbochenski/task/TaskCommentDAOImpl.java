package com.mateuszbochenski.task;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class TaskCommentDAOImpl implements TaskCommentDAO{
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public void add(TaskComment taskComment) {
		em.persist(taskComment);
		
	}

	@Override
	public void delete(TaskComment taskComment) {
		em.remove(em.contains(taskComment)? taskComment : em.merge(taskComment));
	}

	@Override
	public TaskComment update(TaskComment taskComment) {
		return em.merge(taskComment);
	}

	@Override
	public TaskComment findById(long id) {
		return em.find(TaskComment.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TaskComment> retrieveTaskCommentsByTask(Task task) {
		return em.createQuery(
			    "SELECT t FROM TaskComment t where t.task = :task")
				.setParameter("task", task)
			    .getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaskComment> retrieveTaskCommentsByTaskDesc(Task task) {
		return em.createQuery(
			    "SELECT t FROM TaskComment t where t.task = :task order by t.creationDate desc")
				.setParameter("task", task)
			    .getResultList();
	}
	
	

}
