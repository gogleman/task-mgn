package com.mateuszbochenski.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mateuszbochenski.user.User;
import com.mateuszbochenski.user.UserService;

@Controller
public class TaskController {
	
	@Autowired
	TaskService taskService;
	
	@Autowired
	UserService userService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	@RequestMapping(value = "/list-tasks", method = RequestMethod.GET)
	public String listTasks(ModelMap model) {
		
		List<Task> urgentImportantTasks = taskService.retrieveUndoneUndeletedTasksUrgentImportant(1);
		List<Task> urgentNotImportantTasks = taskService.retrieveUndoneUndeletedTasksUrgentNotImportant(1);
		List<Task> notUrgentImportantTasks = taskService.retrieveUndoneUndeletedTasksNotUrgentImportant(1);
		List<Task> notUrgentNotImportantTasks = taskService.retrieveUndoneUndeletedTasksNotUrgentNotImportant(1);
		
		model.addAttribute("tasks",taskService.retrieveUndoneUndeletedTasks(1));
		model.addAttribute("urgentImportantTasks",urgentImportantTasks);
		model.addAttribute("urgentNotImportantTasks",urgentNotImportantTasks);
		model.addAttribute("notUrgentImportantTasks",notUrgentImportantTasks);
		model.addAttribute("notUrgentNotImportantTasks",notUrgentNotImportantTasks);
		
		model.addAttribute("urgentImportantTasksLength", urgentImportantTasks.size());
		model.addAttribute("urgentNotImportantTasksLength", urgentNotImportantTasks.size());
		model.addAttribute("notUrgentImportantTasksLength", notUrgentImportantTasks.size());
		model.addAttribute("notUrgentNotImportantTasksLength", notUrgentNotImportantTasks.size());	

		return "list-tasks";
	}
	
	@RequestMapping(value= "/add-task", method=RequestMethod.GET)
	public String showAddTask(ModelMap model) {
		
		model.addAttribute("task", new Task("","",new Date(),"",false,new Date(),new Date(), false, false, false, null));
		return "add-task";
	}
	
	@RequestMapping(value= "/add-task", method=RequestMethod.POST)
	public String addTask(ModelMap model, @Valid Task task, BindingResult result) {
		if(result.hasErrors()) {
			model.addAttribute("bindingError", "true");
			return "/add-task";
		}
		
		String login = SecurityContextHolder.getContext().getAuthentication().getName(); 
		User currentUser = userService.retreiveUser(login);
		
		Task newTask = new Task(task.getName(),task.getDescription(),new Date(),login,task.isImportant(),task.getPlannedEndDate(),null, false, false, task.isForcedUrgent(), null);
		taskService.add(newTask);
		userService.addTaskToUser(currentUser,newTask);
		model.clear();
		
		return "redirect:list-tasks";
	}
	
	@RequestMapping(value= "/delete-task", method=RequestMethod.GET)
	public String deleteTask(ModelMap model, @RequestParam long id) {
		Task task = taskService.retrieveTask(id);
		taskService.deleteTaskTemporarly(task);
		
		return "redirect:list-tasks";
	}
	
	@RequestMapping(value= "/edit-task", method=RequestMethod.GET)
	public String showEditTask(ModelMap model, @RequestParam long id) {
		Task task = taskService.retrieveTask(id);
		List<TaskComment> taskComments = taskService.retrieveTaskComments(task);
		String currentUserLogin = SecurityContextHolder.getContext().getAuthentication().getName(); 

		model.addAttribute("task", task);
		model.addAttribute("comments",taskComments);
		model.addAttribute("comment",new TaskComment("",currentUserLogin,new Date(),task));
		return "edit-task";
	}
	
	@RequestMapping(value= "/edit-task", method=RequestMethod.POST)
	public String showEditTask(ModelMap model, @Valid Task task, BindingResult result) {
		if(result.hasErrors()) {
			model.addAttribute("bindingError", "true");
			return "/edit-task";	
		}

		taskService.editTask(task);

		model.clear();
		
		return "redirect:list-tasks";
	}
	
	@RequestMapping(value="/complete-task", method=RequestMethod.GET)
	public String completeTask(ModelMap model, @RequestParam long id) {
		Task task = taskService.retrieveTask(id);
		taskService.completeTask(task);
		
		return "redirect:/list-tasks";
	}
	
	@RequestMapping(value="/add-taskcomment",method=RequestMethod.GET)
	public String showTaskComment(ModelMap model, @RequestParam long id) {
		
		Task task = taskService.retrieveTask(id);
		List<TaskComment> taskComments = taskService.retrieveTaskComments(task);
		String currentUserLogin = SecurityContextHolder.getContext().getAuthentication().getName(); 
		TaskComment taskComment = new TaskComment("",currentUserLogin,new Date(),task);

		model.addAttribute("comments",taskComments);
		model.addAttribute("comment",taskComment);
		model.addAttribute("taskName", task.getName());

		return "add-taskComment";
	}

	
	@RequestMapping(value="/add-taskcomment",method=RequestMethod.POST)
	public String addTaskComment(ModelMap model, @Valid @ModelAttribute("comment") TaskComment comment, BindingResult result) {
		
		if(result.hasErrors()) {
			model.addAttribute("bindingError", "true");
			return "/add-taskComment";
			         
		}

		TaskComment newTaskComment = new TaskComment(comment.getText(),comment.getCreatorLogin(),new Date(),comment.getTask());
		taskService.addComment(newTaskComment);
		long id = newTaskComment.getTask().getId();

		model.clear();
		
		return "redirect:/add-taskcomment?id=" +id;
	}


}
