package com.mateuszbochenski.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

@Configuration
public class AuthenticationProviderConfig {
	
	@Bean(name = "dataSource")
	public DriverManagerDataSource dataSource() {
	    DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
	    driverManagerDataSource.setDriverClassName("com.mysql.jdbc.Driver");
	    driverManagerDataSource.setUrl("jdbc:mysql://localhost:3306/taskmng?verifyServerCertificate=false");
	    driverManagerDataSource.setUsername("taskmanageradmin");
	    driverManagerDataSource.setPassword("password");
	    
	    return driverManagerDataSource;
	}

	@Bean(name="userDetailsService")
    public UserDetailsService userDetailsService(){
     JdbcDaoImpl jdbcImpl = new JdbcDaoImpl();
     jdbcImpl.setDataSource(dataSource());
     jdbcImpl.setUsersByUsernameQuery("select login,password, enabled from users where login=?");
     jdbcImpl.setAuthoritiesByUsernameQuery("select u.login, r.name from users u "
 			+ "join users_roles ur on u.user_id = ur.user_id "
 			+ "join roles r on ur.role_id = r.role_id "
 			+ "where u.login=?");
     return jdbcImpl;
    }
}
