package com.mateuszbochenski.archive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mateuszbochenski.task.Task;
import com.mateuszbochenski.task.TaskService;

@Controller
public class ArchiveController {
	
	private final int numberOfShownTasks = 5;
	
	@Autowired
	TaskService taskService;
	
	@RequestMapping(value="/archived-tasks", method= RequestMethod.GET)
	public String showArchive(ModelMap model) {
		
		model.addAttribute("completedtasks",taskService.retrieveDoneUndeletedTasks(1,numberOfShownTasks));
		model.addAttribute("deletedtasks",taskService.retrieveDeletedTasks(1,numberOfShownTasks));
		model.addAttribute("numberOfShownTasks",numberOfShownTasks);
		
		return "archived-tasks";
	}
	
	@RequestMapping(value="/undo-task", method=RequestMethod.GET)
	public String undoTask(ModelMap model, @RequestParam long id) {
		Task task = taskService.retrieveTask(id);
		taskService.uncompleteTask(task);
		
		return "redirect:archived-tasks";
	}
	
	@RequestMapping(value= "/undelete-task", method=RequestMethod.GET)
	public String deleteTask(ModelMap model, @RequestParam long id) {
		Task task = taskService.retrieveTask(id);
		taskService.undeleteTaskTemporarly(task);
		taskService.uncompleteTask(task);

		return "redirect:archived-tasks";
	}
	
	@RequestMapping(value= "/delete-done-task", method=RequestMethod.GET)
	public String deleteDoneTask(ModelMap model, @RequestParam long id) {
		Task task = taskService.retrieveTask(id);
		taskService.deleteTaskTemporarly(task);
		return "redirect:archived-tasks";
	}
	
	@RequestMapping(value= "/delete-task-permanently", method=RequestMethod.GET)
	public String deleteTaskPermanently(ModelMap model, @RequestParam long id) {
		Task task = taskService.retrieveTask(id);
		taskService.deleteTaskPermanently(task);
		return "redirect:archived-tasks";
	}
	
}

