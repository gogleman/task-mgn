//Highlights current tab in menu
$(document).ready(function() {
  $('li.active').removeClass('active');
  $('a[href="' + location.pathname + '"]').closest('li').addClass('active'); 
});

//Removes logout button from login page
$(function(){
	if($(".login")[0]){
		var logout = document.getElementById("logout");
		var currentUser = document.getElementById("currentUser");
		if (logout) {
			logout.parentNode.removeChild(logout);
		}
		if (currentUser) {
			currentUser.parentNode.removeChild(currentUser);
		}

	}
});