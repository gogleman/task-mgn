<%@ include file="common/header.jspf"%>
<%@ include file="common/menu.jspf"%>

<div class="container">

	<jsp:useBean id="yesterday" class="java.util.Date" />
	<jsp:setProperty name="yesterday" property="time"
		value="${yesterday.time - 86400000}" />

	<h1>Home</h1>
	<h2>
		<span class="glyphicon glyphicon-record"></span> All tasks
	</h2>

	<br>
	<div class="row">

		<!-- Urgent important tasks -->
		<div class="col-sm-5 col-md-6">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse1"><span
							class="glyphicon glyphicon-fire"></span>&nbsp Important / Urgent
							tasks <span class="badge">${urgentImportantTasksLength}</span></a>
					</h4>
				</div>

			</div>
		</div>


		<!-- NotUrgent important tasks -->
		<div class="col-sm-5 col-md-6">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse3"><span
							class="glyphicon glyphicon-send"></span>&nbsp Important / Not
							urgent tasks <span class="badge">${notUrgentImportantTasksLength}</span></a>
					</h4>
				</div>
			</div>
		</div>
	</div>
	<div class="row">

		<!-- Urgent unimportant tasks -->
		<div class="col-sm-5 col-md-6">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse2"><span
							class="glyphicon glyphicon-bell"></span>&nbsp Unimportant /
							Urgent tasks <span class="badge">${urgentNotImportantTasksLength}</span></a>
					</h4>
				</div>
			</div>
		</div>

		<!-- NotUrgent unimportant tasks -->
		<div
			class="col-sm-6 col-md-5 col-md-offset-2 col-lg-6 col-lg-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse4"><span
							class="glyphicon glyphicon-hourglass"></span>&nbsp Unimportant /
							Not urgent tasks <span class="badge">${notUrgentNotImportantTasksLength}</span></a>
					</h4>
				</div>

			</div>
		</div>
	</div>

	<!-- Tasks' lists -->
	<!-- Urgent important tasks -->
	<div class="panel-group" id="accordion">
		<div id="collapse1" class="panel-collapse collapse">
			<div class="panel-body">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class="col-sm-3"><span class="glyphicon glyphicon-fire"></span>&nbsp
								Name</th>
							<th class="col-sm-4">Description</th>
							<th class="col-sm-1">Planned end date</th>
							<th class="col-sm-1">Creator</th>
							<th class="col-sm-3">Actions</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${urgentImportantTasks}" var="task">
							<tr>
								<td><span class="glyphicon glyphicon-chevron-right"></span>
									<c:if test="${yesterday gt task.plannedEndDate}">
										<span class="overdueCheck"> ! </span>
									</c:if> ${task.name}</td>
								<td>${task.description}</td>
								<td><fmt:formatDate pattern="dd/MM/yyyy"
										value="${task.plannedEndDate}" /></td>

								<td>${task.creatorLogin}</td>
								<td><a href="/complete-task?id=${task.id}"
									class="btn btn-success">Done</a> <a
									href="/edit-task?id=${task.id}" class="btn btn-info">Edit</a> <a
									href="/add-taskcomment?id=${task.id}" class="btn btn-default">Info</a>
									<a href="/delete-task?id=${task.id}" class="btn btn-danger">Delete</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

		<!-- NotUrgent important tasks -->
		<div id="collapse3" class="panel-collapse collapse">
			<div class="panel-body">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class="col-sm-3"><span class="glyphicon glyphicon-send"></span>&nbsp
								Name</th>
							<th class="col-sm-4">Description</th>
							<th class="col-sm-1">Planned end date</th>
							<th class="col-sm-1">Creator</th>
							<th class="col-sm-3">Actions</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${notUrgentImportantTasks}" var="task">
							<tr>
								<td><span class="glyphicon glyphicon-chevron-right"></span>
									<span class="overdueCheck"> <c:if
											test="${yesterday gt task.plannedEndDate}">!</c:if>
								</span> ${task.name}</td>
								<td>${task.description}</td>
								<td><fmt:formatDate pattern="dd/MM/yyyy"
										value="${task.plannedEndDate}" /></td>
								<td>${task.creatorLogin}</td>
								<td><a href="/complete-task?id=${task.id}"
									class="btn btn-success">Done</a> <a
									href="/edit-task?id=${task.id}" class="btn btn-info">Edit</a> <a
									href="/add-taskcomment?id=${task.id}" class="btn btn-default">Info</a>
									<a href="/delete-task?id=${task.id}" class="btn btn-danger">Delete</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>


		<!-- Urgent unimportant tasks -->
		<div id="collapse2" class="panel-collapse collapse">
			<div class="panel-body">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class="col-sm-3"><span class="glyphicon glyphicon-bell"></span>&nbsp
								Name</th>
							<th class="col-sm-4">Description</th>
							<th class="col-sm-1">Planned end date</th>
							<th class="col-sm-1">Creator</th>
							<th class="col-sm-3">Actions</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${urgentNotImportantTasks}" var="task">
							<tr>
								<td><span class="glyphicon glyphicon-chevron-right"></span>
									<span class="overdueCheck"> <c:if
											test="${yesterday gt task.plannedEndDate}">!</c:if>
								</span> ${task.name}</td>
								<td>${task.description}</td>
								<td><fmt:formatDate pattern="dd/MM/yyyy"
										value="${task.plannedEndDate}" /></td>
								<td>${task.creatorLogin}</td>
								<td><a href="/complete-task?id=${task.id}"
									class="btn btn-success">Done</a> <a
									href="/edit-task?id=${task.id}" class="btn btn-info">Edit</a> <a
									href="/add-taskcomment?id=${task.id}" class="btn btn-default">Info</a>
									<a href="/delete-task?id=${task.id}" class="btn btn-danger">Delete</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

		<!-- NotUrgent unimportant tasks -->
		<div id="collapse4" class="panel-collapse collapse">
			<div class="panel-body">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th class="col-sm-3"><span
								class="glyphicon glyphicon-hourglass"></span> <span
								class="overdueCheck"> <c:if
										test="${yesterday gt task.plannedEndDate}">!</c:if>
							</span> &nbsp Name</th>
							<th class="col-sm-4">Description</th>
							<th class="col-sm-1">Planned end date</th>
							<th class="col-sm-1">Creator</th>
							<th class="col-sm-3">Actions</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${notUrgentNotImportantTasks}" var="task">
							<tr>
								<td><span class="glyphicon glyphicon-chevron-right"></span>
									${task.name}</td>
								<td>${task.description}</td>
								<td><fmt:formatDate pattern="dd/MM/yyyy"
										value="${task.plannedEndDate}" /></td>
								<td>${task.creatorLogin}</td>
								<td><a href="/complete-task?id=${task.id}"
									class="btn btn-success">Done</a> <a
									href="/edit-task?id=${task.id}" class="btn btn-info">Edit</a> <a
									href="/add-taskcomment?id=${task.id}" class="btn btn-default">Info</a>
									<a href="/delete-task?id=${task.id}" class="btn btn-danger">Delete</a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- Add button -->
	<div>
		<a href="/add-task" class="btn btn-success pull-right">Add task</a>
	</div>

</div>

<%@ include file="common/footer.jspf"%>