<%@ include file="common/header.jspf"%>
<%@ include file="common/menu.jspf"%>

<div class="container">

	<h1>Archive</h1>
	<br>
	<h2><span class="glyphicon glyphicon-record"></span> Recently done tasks</h2>
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Final end date</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody class="text-success">
			<c:forEach items="${completedtasks}" var="task">
				<tr>
					<td><span class="glyphicon glyphicon-ok"></span> ${task.name}</td>
					<td>${task.description}</td>
					<td><fmt:formatDate pattern="dd/MM/yyyy"
							value="${task.finalEndDate}" /></td>
					<td><a href="/undo-task?id=${task.id}" class="btn btn-info">Undo</a>
						<a href="/delete-done-task?id=${task.id}" class="btn btn-danger">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
		<tfoot>
			<tr>
				<td><i>Here are shown <b>${numberOfShownTasks}</b> recent
						tasks
				</i></td>
			</tr>
		</tfoot>

	</table>

	<h2><span class="glyphicon glyphicon-menu-right"></span>Recently deleted tasks</h2>

	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody class="text-muted tasks-deleted">
			<c:forEach items="${deletedtasks}" var="task">
				<tr>
					<td><span class="glyphicon glyphicon-remove"></span>
						${task.name}</td>
					<td>${task.description}</td>
					<td><a href="/undelete-task?id=${task.id}"
						class="btn btn-info">Undelete</a></td>
				</tr>
			</c:forEach>
		</tbody>
		<tfoot>
			<tr>
				<td><i>Here are shown <b>${numberOfShownTasks}</b> recent
						tasks
				</i></td>
			</tr>
		</tfoot>
	</table>


	<%@ include file="common/footer.jspf"%>