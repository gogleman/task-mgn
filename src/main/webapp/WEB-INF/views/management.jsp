<%@ include file="common/header.jspf"%>
<%@ include file="common/menu.jspf"%>

<div class="container">
	<h1>Management</h1>
	<div>
		<h2>
			<span class="glyphicon glyphicon-record"></span> Users
		</h2>

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="col-sm-2">Login</th>
					<th class="col-sm-2">Name</th>
					<th class="col-sm-2">Surname</th>
					<th class="col-sm-3">E-mail</th>
					<th class="col-sm-3">Actions</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${users}" var="user">
					<tr>
						<td><span class="glyphicon glyphicon-chevron-right"></span>
							${user.login}</td>
						<td>${user.firstName}</td>
						<td>${user.lastName}</td>
						<td>${user.email}</td>
						<td><c:if test="${user.enabled eq true}">
								<a href="/activate-user?id=${user.id}"
									class="btn btn-success disabled" tabindex="-1">Activate</a>
								<a href="/deactivate-user?id=${user.id}" class="btn btn-danger">Deactivate</a>
							</c:if> <c:if test="${user.enabled eq false}">
								<a href="/activate-user?id=${user.id}" class="btn btn-success">Activate</a>
								<a href="/deactivate-user?id=${user.id}"
									class="btn btn-danger disabled" tabindex="-1">Deactivate</a>
							</c:if></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>

</div>

<%@ include file="common/footer.jspf"%>