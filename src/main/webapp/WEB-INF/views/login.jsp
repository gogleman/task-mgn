<%@ include file="common/header.jspf"%>
<%@ include file="common/menu.jspf"%>

<div class="container login">
	<h1>Login</h1>
	<br>
	<div class="row">
		<div class="col-md-6 col-md-offset-5">
			<form class="form-signin" action="/login" method="post">
				<fieldset class="form-group">
					<label for="username" class="sr-only">User name</label> <input
						type="text" name="username" placeholder="Username" required
						autofocus />
				</fieldset>
				<fieldset class="form-group">
					<label for="username" class="sr-only">Password</label> <input
						type="password" name="password" placeholder="Password" required />
				</fieldset>

				<input type="submit" value="Login" class="btn btn-info" /> <input
					type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2 col-md-offset-5">
			<br>
			<c:if test="${not empty param.error}">
				<div id="error" class="bg-danger">Wrong credentials</div>
			</c:if>

		</div>
	</div>
</div>

<%@ include file="common/footer.jspf"%>