<%@ include file="common/header.jspf"%>
<%@ include file="common/menu.jspf"%>

<div class="container">
	<div>
		<h2>
			<span class="glyphicon glyphicon-record"></span> Add new comment
			-<i> ${taskName} </i>
		</h2>
		<br>
		<form:form method="post" modelAttribute="comment">
			<form:hidden path="id" />
			<form:hidden path="creatorLogin" />
			<form:hidden path="creationDate" />
			<form:hidden path="task.id" />
			<fieldset class="form-group">
				<form:label path="text">Text </form:label>
				<form:input path="text" type="text" class="form-control" />
				<form:errors path="text" cssClass="text-warning"></form:errors>
			</fieldset>

			<c:if test="${not empty bindingError}">
				<div id="error" class="bg-danger">Incorrect data. Please fix
					data and hit Apply changes again</div>
				<br>
			</c:if>

			<input type="submit" value="Add comment"
				class="btn btn-success pull-right" />
			<a href="/list-tasks" class="btn btn-default pull-right btn-space">Cancel</a>
		</form:form>
	</div>
	<div>
		<br>
		<br>
		<%@ include file="common/comments.jspf"%>
	</div>
</div>

<%@ include file="common/footer.jspf"%>