<%@ include file="common/header.jspf"%>
<%@ include file="common/menu.jspf"%>

<div class="container">

	<c:if test="${not empty settingsSaved}">
		<div id="error" class="bg-success">Settings saved</div>
	</c:if>

	<c:if test="${not empty bindingError}">
		<div id="error" class="bg-danger">Incorrect data. Please fix
			data and hit Save settings again</div>
	</c:if>
	<h1>Settings</h1>

	<h2>
		<span class="glyphicon glyphicon-record"></span> Personal settings
	</h2>
	<form:form method="post" modelAttribute="settingsWrapper">
		<form:hidden path="user.id" />
		<form:hidden path="user.login" />
		<form:hidden path="user.password" />
		<form:hidden path="user.enabled" />
		<form:hidden path="user.state" />
		<fieldset>
			<form:label path="user.firstName">Name</form:label>
			<form:input path="user.firstName" type="text" class="form-control" />
			<form:errors path="user.firstName" cssClass="text-warning" />
		</fieldset>
		<fieldset>
			<form:label path="user.lastName">Surname</form:label>
			<form:input path="user.lastName" type="text" class="form-control" />
			<form:errors path="user.lastName" cssClass="text-warning" />
		</fieldset>
		<fieldset>
			<form:label path="user.email">Email</form:label>
			<form:input path="user.email" type="text" class="form-control" />
			<form:errors path="user.email" cssClass="text-warning" />
		</fieldset>
		<br>
		<h2>
			<span class="glyphicon glyphicon-record"></span> Application
			settings
		</h2>

		<form:hidden path="setting.id" />
		<form:hidden path="setting.type" />
		<form:hidden path="setting.user.id" />
		<form:hidden path="setting.valueString" />

		<fieldset>
			<form:label path="setting.valueInt">Number of urgent days</form:label>
			<form:input path="setting.valueInt" type="text" class="form-control" />
			<i>(Tells how many days before planned end date the task becomes
				urgent. Must be a number between 0 and 365)</i><br>
			<form:errors path="setting.valueInt" cssClass="text-warning" />
		</fieldset>

		<input type="submit" value="Save settings"
			class="btn btn-success pull-right" id="settingsSubmit" />
		<a href="/list-tasks" class="btn btn-default pull-right btn-space">Cancel</a>
	</form:form>
</div>

<%@ include file="common/footer.jspf"%>