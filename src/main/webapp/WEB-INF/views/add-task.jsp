<%@ include file="common/header.jspf"%>
<%@ include file="common/menu.jspf"%>

<div class="container">

	<c:if test="${not empty bindingError}">
		<div id="error" class="bg-danger">Incorrect data. Please fix
			data and hit Add again</div>
	</c:if>

	<h2><span class="glyphicon glyphicon-record"></span> Add task</h2>

	<form:form method="post" modelAttribute="task"
		cssClass="form-horizontal">
		<fieldset class="form-group">
			<form:label path="name">Name * </form:label>
			<form:input path="name" type="text" class="form-control"
				required="required" />
			<form:errors path="name" cssClass="text-warning"></form:errors>
		</fieldset>
		<fieldset class="form-group">
			<form:label path="description">Description </form:label>
			<form:input path="description" type="text" class="form-control" />
			<form:errors path="description" cssClass="text-warning"></form:errors>
		</fieldset>
		<fieldset class="form-group">
			<form:label path="plannedEndDate">Planned end date * </form:label>
			<form:input path="plannedEndDate" type="date" class="form-control"
				required="required" format="dd/mm/yyyy" />
			<form:errors path="plannedEndDate" cssClass="text-warning"></form:errors>
		</fieldset>
		<fieldset class="form-group">

			<form:label path="important" cssClass="checkbox">Status</form:label>
			<form:checkbox path="important" />
			Important
		</fieldset>

		<fieldset class="form-group">
			<form:label path="forcedUrgent" cssClass="checkbox">Force urgent</form:label>
			<form:checkbox path="forcedUrgent" />
			Force urgent <i>(Task will be set to urgent regardless the
				planned end date)</i>
		</fieldset>
		<input type="submit" value="Add" class="btn btn-success pull-right" />
		<a href="/list-tasks" class="btn btn-default pull-right btn-space">Cancel</a>
	</form:form>

</div>

<%@ include file="common/footer.jspf"%>