<%@ include file="common/header.jspf"%>
<%@ include file="common/menu.jspf"%>
<div class="container">
	<h2>HTTP Status 403 - Access is denied</h2>
	You do not have permission to access this page
</div>
<%@ include file="common/footer.jspf"%>